Demo showing a `docker-compose` application.  This application consists of two container images:
- nginx/unit:1.15.0-full
- mariadb:10.4

# docker-compose.yml
- [version](https://docs.docker.com/compose/compose-file/)
- [services](https://docs.docker.com/compose/compose-file/compose-versioning/)
- [networks](https://docs.docker.com/compose/networking/)
- [nginx/unit](https://hub.docker.com/r/nginx/unit)
- [maria](https://hub.docker.com/_/mariadb)

# Dockerfile 
Nginx/unit comes with php:7.3 but is missing the necessary mysqli component. Under the *unit* directory you will find the dockerfile referenced in the `docker-compolse.yml` .

# web.conf
Using this file to set up the upstream

# init.sql
In order to automate this build, the `sql.init` is used to create:
- user & password
- database
- table

# config.json
Here you define your application properties:
- listener (9000)
- routes
   - action
      - pass - routes to target in case of match
	  - share - in case of match Unit serves the files at this path
- language (php)
- root directory (/www/root)
- index (index.php)

# docker_clean.sh
Shell file to clean environment.  [Clean up](#clean-up)

# Steps to run
`docker-compose up -d`

Once maria is up and ready, navigate to http://localhost:8001 (this should fail). We must initialize the demo object.
Before we initialize, lets see what is set up:<br/>
`docker exec -it unit_web curl --unix-socket ./var/run/control.unit.sock http://localhost`
```
{
	"certificates": {},
	"config": {
		"listeners": {},
		"applications": {}
	}
}
```
Configure 'demo' object:<br/>
`docker exec -it unit_web curl -X PUT -d @/root/json/config.json -H "Content-type: application/json" --unix-socket /var/run/control.unit.sock http://localhost/config`

```
{
    "listeners": {
        "*:9000": {
            "pass": "routes"
        }
     },

     "applications": {
        "demo": {
            "type": "php",
            "root": "/www/root/html", 
            "index": "index.php"
        }
    },
    "routes": [
        {
            "match": {
                "uri": [
                    "*.css",
                    "*.html"
                ]
            },
            "action": {
                "share": "/www/root/html/static"
            }
        },
             {
                 "action": {
                     "pass": "applications/demo"
                 }
             }
    ]
}

```

Now go back and refresh your browser (http://localhost:8001)

To delete the unit object *demo*:<br />
We must first delete the listener:<br />
`docker exec -it unit_web curl -X DELETE --unix-socket /var/run/control.unit.sock http://localhost/config/listeners/*:9000`

```
{
	"success": "Reconfiguration done."
}
```
Then remove the config:<br />
`docker exec -it unit_web curl -X DELETE --unix-socket /var/run/control.unit.sock http://localhost/config/applications/demo`

```
{
	"success": "Reconfiguration done."
}
```
Now try and refresh your page

# <a name="clean-up"></a> Clean up 
To remove containers, images and existing db: 


From mac: `sh docker_clean.sh`<br />
From ubuntu: `sudo su -c'./docker_clean.sh'`


# Video Demo 

[![Video](http://img.youtube.com/vi/Qw9zlE3t8Ko/0.jpg)](https://www.youtube.com/watch?v=Qw9zlE3t8Ko)