#!/bin/bash

#Container Scalar
dock1="unit_web"
dock2="maria"

#Images Scalar
image1="nginx/unit:latest"
image2="mariadb:latest"

#db scalar
db="./sql/dbdata"

#base nginx/unit image
nu="nginx/unit"

for c in $dock1 $dock2
do
  docker rm -f $c > /dev/null 2>&1
done

echo "Containers removed"

for i in $image1 $image2
 do 
   docker rmi $i > /dev/null 2>&1
   echo "Removed $i"
done

#if Linux, the dbdata is owned by root; assing local user as owner
if [[ $OSTYPE == "linux-gnu" ]]; then
own=$(ls -ld sql/dbdata | awk '{ print $4}')
  if [[ $own == "root" ]];  then
    chown -R $USER sql/dbdata
  fi 
fi

#delete dbdata directory
rm -fr $db
del=$?
if [ $del -eq 0 ];  then
  echo "dbdata is bye bye"
else
  echo "dbdata is PRESENT"
fi


unit=$(docker images $nu -q)
docker rmi $unit > /dev/null 2>&1
res=$?
if [ $res -eq 0 ]; then
  echo "Work completed"
fi

echo "We are out of here, bye"
sleep 2
clear