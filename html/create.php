<?php
include_once('mysql.php');

$ins = "CREATE TABLE IF NOT EXISTS test (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR (10) NOT NULL,
    lastname VARCHAR (10) NOT NULL,
    phone VARCHAR (15),
    dt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)";

if ($con -> query($ins) === TRUE) {
    echo "Table created";
} else {
    echo "Tabel creation failed: " . $con->error;
}
$con -> close();
?>